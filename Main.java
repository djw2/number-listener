/*
    Main class for server execution.
    Arguments: None

 */
public class Main {

    private static Server server;

    public static void main(String[] args) {

        server = new Server(4000);
        System.out.println("Starting up server ....");
        server.start();

        while (server.getActive() == true) {
            //Loop while server is running
        }
        System.out.println("Server terminating. Goodbye");
    }


}





